<?php

/**
 * @file
 * Provides preprocess logic and other functionality for theming.
 */

// Includes:
$theme_path = drupal_get_path('theme', 'bootstrap');
require_once $theme_path . '/includes/menu.inc';
// require_once $theme_path . '/includes/bootstrap.inc';
// require_once $theme_path . '/includes/theme.inc';
// require_once $theme_path . '/includes/pager.inc';
// require_once $theme_path . '/includes/form.inc';
// require_once $theme_path . '/includes/admin.inc';

// Load module specific files in the modules directory.
$includes = file_scan_directory($theme_path . '/includes/modules', '/\.inc$/');
foreach ($includes as $include) {
  if (module_exists($include->name)) {
    require_once $include->uri;
  }
}

/**
 * Implements hook_FORM_ID_form_alter().
 */
function bootstrap_form_menu_edit_item_alter(&$form, &$form_state) {
  // Adds additional text to assist with formatting menu links.
  $form['link_title']['#description'] .= t(' <strong>You may also use
    "!divider" for divider or prepend "!nav_header" to designate nav-header.
    </strong>',
    array(
      '!divider' => '---',
      '!nav_header' => '#',
    )
  );
}

/**
 * Implements hook_theme().
 */
function bootstrap_theme(&$existing, $type, $theme, $path) {
  return array(
    'brand' => array(
      'file' => 'includes/theme.inc',
      'variables' => array(
        'name' => NULL,
        'href' => NULL,
        'logo' => NULL,
      ),
    ),
    'preface' => array(
      'file' => 'includes/theme.inc',
      'variables' => array(
        'breadcrumb' => NULL,
        'title_prefix' => array(),
        'title' => NULL,
        'title_suffix' => array(),
        'help' => array(),
        'tabs' => array(),
        'actions' => array(),
      ),
    ),
    'copyright' => array(
      'file' => 'includes/theme.inc',
      'variables' => array(
        'name' => NULL,
      ),
    ),
    'navbar_toggler' => array(
      'file' => 'includes/theme.inc',
    ),
    'bootstrap_links' => array(
      'variables' => array(
        'links' => array(),
        'attributes' => array(),
        'heading' => NULL
      ),
    ),
    'bootstrap_btn_dropdown' => array(
      'variables' => array(
        'links' => array(),
        'attributes' => array(),
        'type' => NULL
      ),
    ),
  );
}

/**
 * Implements hook_process_page().
 */
function bootstrap_process_page(&$vars) {
  // Provide additional variables to theme.
  $vars['brand'] = theme('brand', array(
    'name' => $vars['site_name'],
    'href' => $vars['front_page'],
    'logo' => $vars['logo'],
  ));

  $vars['preface'] = theme('preface', array(
    'breadcrumb' => $vars['breadcrumb'],
    'title_prefix' => $vars['title_prefix'],
    'title' => $vars['title'],
    'title_suffix' => $vars['title_suffix'],
    'messages' => $vars['messages'],
    'help' => $vars['page']['help'],
    'tabs' => $vars['tabs'],
    'actions' => $vars['action_links'],
  ));

  $vars['navbar_toggler'] = theme('navbar_toggler');
  $vars['copyright'] = theme('copyright', array('name' => $vars['site_name']));

  // Render navbar menu
  $vars['navbar_menu'] = drupal_render($vars['navbar_menu_tree']);

  // Render and clean up navbar search form.
  $vars['navbar_search'] = drupal_render($vars['navbar_search_form']);
  $vars['navbar_search'] = strip_tags($vars['navbar_search'], '<form><input>');

  // Prepare some useful variables.
  $vars['has_header'] = !empty($vars['title']) || !empty($vars['messages']);
  $vars['has_sidebar_first'] = !empty($vars['page']['sidebar_first']) || !empty($vars['page']['sidebar_first_affix']);
  $vars['has_sidebar_second'] = !empty($vars['page']['sidebar_second']) || !empty($vars['page']['sidebar_second_affix']);
  $vars['content_cols'] = 24 - 3 * (int) $vars['has_sidebar_first'] - 3 * (int) $vars['has_sidebar_second'];
}

/**
 * Preprocess variables for page.tpl.php
 *
 * @see page.tpl.php
 */
function bootstrap_preprocess_page(&$vars) {
  // Prepare navbar menu
  $vars['navbar_menu_tree'] = menu_tree(variable_get('menu_main_links_source', 'main-menu'));

  // Prepare navbar search form
  $vars['navbar_search_form'] = FALSE;
  if (module_exists('search')) {
    $form = drupal_get_form('_bootstrap_search_form');
    $form['#attributes']['class'][] = 'navbar-search';
    $form['#attributes']['class'][] = 'navbar-search-elastic';
    $form['#attributes']['class'][] = 'hidden-phone';
    $vars['navbar_search_form'] = $form;
  }

  // Load Bootstrap Library
  _bootstrap_load_library();

  // Affix Sidebars.
  $theme_path = drupal_get_path('theme', 'bootstrap');
  drupal_add_js($theme_path . '/assets/js/affix.js');
}

/**
 * Implements hook_preprocess_block().
 */
function bootstrap_preprocess_block(&$vars, $hook) {
  if ($vars['block']->region == 'featured') {
    $vars['classes_array'][] = 'item';
    if ($vars['block_id'] == 1) {
      $vars['classes_array'][] = 'active';
    }
  }
  // Use a bare template for the page's main content.
  if ($vars['block_html_id'] == 'block-system-main') {
    $vars['theme_hook_suggestions'][] = 'block__no_wrapper';
  }
  $vars['title_attributes_array']['class'][] = 'block-title';
}

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function bootstrap_process_block(&$variables, $hook) {
  // Drupal 7 should use a $title variable instead of $block->subject.
  $variables['title'] = $variables['block']->subject;
}

/**
 * Implements hook_preprocess_poll_bar().
 */
function bootstrap_preprocess_poll_bar(&$vars) {
  $vars['theme_hook_suggestions'] = array('poll_bar');
}

/**
 * Implements hook_preprocess_button().
 */
function bootstrap_preprocess_button(&$vars) {
  $vars['element']['#attributes']['class'][] = 'btn';
  if (isset($vars['element']['#parents'][0]) && $vars['element']['#parents'][0] == 'submit') {
    $vars['element']['#attributes']['class'][] = 'btn-primary';
  }
}

/**
 * Implements hook_process_table().
 */
function bootstrap_process_table(&$vars) {
  $vars['attributes']['class'] = array();
  $vars['attributes']['class'][] = 'table';
  $vars['attributes']['class'][] = 'table-striped';
  $vars['attributes']['class'][] = 'table-bordered';
}

/**
 * Implements hook_preprocess_menu_local_tasks().
 */
function bootstrap_preprocess_menu_local_tasks(&$vars) {
  $vars['primary_widget'] = 'tabs';
  $vars['secondary_widget'] = 'pills';
}

/**
 * Implements theme_menu_local_tasks().
 */
function bootstrap_menu_local_tasks(&$vars) {
  $result = '';

  if (!empty($vars['primary'])) {
    $vars['primary']['#prefix'] = '<ul class="nav nav-' . $vars['primary_widget'] . '">';
    $vars['primary']['#suffix'] = '</ul>';
    $result .= drupal_render($vars['primary']);
  }
  if (!empty($vars['secondary'])) {
    $vars['secondary']['#prefix'] = '<ul class="nav nav-' . $vars['secondary_widget'] . '">';
    $vars['secondary']['#suffix'] = '</ul>';
    $result .= drupal_render($vars['secondary']);
  }

  return $result;
}

/**
 * Implements hook_status_messages().
 */
function bootstrap_status_messages(&$vars) {
  $output = '';
  $status = array(
    'status' => 'alert-success',
    'warning' => 'alert-block',
    'error' => 'alert-error',
  );

  foreach (drupal_get_messages($vars['display']) as $type => $messages) {
    $output .= '<div class="messages alert ' . $status[$type] . '" data-alert="alert">';

    foreach ($messages as $message) {
      $output .= $message . '<br>';
    }
    $output .= '</div>';
  }

  return $output;
}


/**
 * Helper function: returns a navbar search form.
 */
function _bootstrap_search_form($form, &$form_state) {
  $form = search_form($form, $form_state);

  // Set additional attributes.
  $form['basic']['keys']['#title'] = '';
  $form['basic']['keys']['#attributes']['class'][] = 'search-query';
  $form['basic']['keys']['#attributes']['placeholder'] = t('Search');

  // Unset unnecessary data and attributes.
  unset($form['basic']['submit']);
  unset($form['basic']['#type']);
  unset($form['basic']['#attributes']);

  return $form;
}

/**
 * Helper function: loads Twitter Bootstrap library.
 */
function _bootstrap_load_library() {
  // Connect Twitter Bootstrap via Libraries API.
  $loaded = FALSE;
  if (module_exists('libraries')) {
    if (libraries_detect('bootstrap')) {
      // Try to load the library.
      $loaded = libraries_load('bootstrap');
    }
    if (!$loaded) {
      // Otherwise try to load the library manually by path.
      $bootstrap_path = libraries_get_path('bootstrap');
      if ($bootstrap_path) {
        drupal_add_css($bootstrap_path . '/css/bootstrap.css', array('media' => 'all'));
        drupal_add_css($bootstrap_path . '/css/bootstrap-responsive.css', array('media' => 'screen'));
        drupal_add_js($bootstrap_path . '/js/bootstrap.js');
        $loaded = TRUE;
      }
    }
  }
  if (!$loaded) {
    // Notify a user if Bootstrap library was not found.
    drupal_set_message(t('Twitter Bootstrap library was not found. See <a href="http://drupal.org/project/bootstrap#install" target="_blank">installation guide</a> for more details.'), 'error');
  }
} // bootstrap_load_library()
